﻿using System;
using System.Threading;

namespace KeyStrokeGenerator
{
	internal class MouseMoving
	{
		private const int MULTIPLIER = 10;

		public static void DoMouseMovingTest()
		{

			int y = 1, x = 1;
			for ( var i = 0; i < 100; i++ )
			{
				if ( i % 8 == 0 )
				{
					//simulate the user moving his / her finger back to the original position
					Thread.Sleep( 1000 );
				}

				//Move up or down
				if ( i % 64 == 0 )
				{
					y *= -1;
				}

				if ( i % 64 == 32 )
				{
					x *= -1;
				}

				MoveMouse( 1 * x, 1 * y );
				Thread.Sleep( 50 );
			}
		}

		private static void MoveMouse( int dXMove, int dYMove )
		{
			var inputs = new[]
				{
					new Structs.INPUT
						{
							type = (uint) Enums.INPUTTYPES.MOUSE,
							U = new Structs.InputUnion( )
								{
									mi = new Structs.MOUSEINPUT( )
										{
											dx = dXMove * MULTIPLIER,
											dy = dYMove * MULTIPLIER,
											dwFlags = Enums.MOUSEEVENTF.MOVE,
										}
								}
						}
				};

			User32.SendInput( 1u, inputs, Structs.INPUT.Size );
		}
	}
}