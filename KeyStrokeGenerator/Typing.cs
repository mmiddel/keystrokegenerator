﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace KeyStrokeGenerator
{
	class Typing
	{
		/// <summary>
		///    http://en.wikipedia.org/wiki/Words_per_minute
		///    In one study of average computer users in 1998,
		///    the average rate for transcription was 33 words per minute,
		///    and 19 words per minute for composition.
		///    In the same study, when the group was divided into "fast," "moderate," and "slow" groups,
		///    the average speeds were 40 wpm, 35 wpm, and 23 wpm, respectively.
		///    Kluwer typists are "fast", meaning the type at 40 WPM * 5 Char per Word = 200 chars per minute
		/// </summary>
		private const int fastTypistSpeed = 400 * 5;

		/// <summary>
		///    The time to wait between typing two characters, in miliseconds
		/// </summary>
		private const int interCharWaitingTime = (int) ( ( 60.0 / fastTypistSpeed ) * 1000 );

		/// <summary>
		///    http://shakespeare.mit.edu/cymbeline/cymbeline.1.2.html
		/// </summary>
		private const string Shakespeare = @"Sir, I would advise you to shift a shirt; the
violence of action hath made you reek as a
sacrifice: where air comes out, air comes in:
there's none abroad so wholesome as that you vent.©
CLOTEN
If my shirt were bloody, then to shift it. Have I hurt him?
Second Lord
[Aside] No, 'faith; not so much as his patience.
First Lord
Hurt him! his body's a passable carcass, if he be
not hurt: it is a thoroughfare for steel, if it be not hurt.
Second Lord
[Aside] His steel was in debt; it went o' the
backside the town.
CLOTEN
The villain would not stand me.
Second Lord
[Aside] No; but he fled forward still, toward your face.
First Lord
Stand you! You have land enough of your own: but
he added to your having; gave you some ground.
Second Lord
[Aside] As many inches as you have oceans. Puppies!
CLOTEN
I would they had not come between us.
Second Lord
[Aside] So would I, till you had measured how long
a fool you were upon the ground.
CLOTEN
And that she should love this fellow and refuse me!
Second Lord
[Aside] If it be a sin to make a true election, she
is damned.";

		public static void DoTypingTest()
		{
			Console.Write( "starting typing a {3} character text at {0} miliseconds per character, meaning a CPM of {1}, meaning a WPM of {2}",
								interCharWaitingTime, fastTypistSpeed, fastTypistSpeed / 5, Shakespeare.Length );

			char[] shakespeareCharArray = Shakespeare.ToCharArray();
			int length = shakespeareCharArray.Length;

			for ( int i = 0; i < length; i++ )
			{
				Console.WriteLine( i );
				if ( i % 128 == 0 )
				{
					Thread.Sleep( 900 );
				}

				WriteChar( shakespeareCharArray[ i ] );

				Thread.Sleep( interCharWaitingTime );
			}
		}

		private static void WriteChar( char characterToWrite )
		{
			string characterEnumValue;

			if ( char.IsLetter( characterToWrite ) )
				characterEnumValue = "KEY_" + ( characterToWrite.ToString().ToUpper() );
			else if ( characterToWrite == ' ' )
				characterEnumValue = "SPACE";
			else if ( characterToWrite == '\n' )
				characterEnumValue = "RETURN";
			else
				return;
			var scanCode = (Enums.ScanCodeShort) Enum.Parse( typeof( Enums.ScanCodeShort ), characterEnumValue );
			var virtualKeyCode = (Enums.VirtualKeyShort) Enum.Parse( typeof( Enums.VirtualKeyShort ), characterEnumValue );
			var inputs = new[]
				{
					new Structs.INPUT
						{
							type = (uint) Enums.INPUTTYPES.KEYBOARD,
							U = new Structs.InputUnion( )
								{
									ki = new Structs.KEYBDINPUT
										{
											wScan = scanCode,
											wVk = virtualKeyCode
										}
								}
						},

					new Structs.INPUT
						{
							type = (uint) Enums.INPUTTYPES.KEYBOARD,
							U = new Structs.InputUnion( )
								{
									ki = new Structs.KEYBDINPUT( )
										{
											wScan = scanCode,
											wVk = virtualKeyCode,
											dwFlags = Enums.KEYEVENTF.KEYUP
										}
								}
						}
				};

			User32.SendInput( 2u, inputs, Structs.INPUT.Size );
		}
	}
}
