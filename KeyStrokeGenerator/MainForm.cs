﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace KeyStrokeGenerator
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent( );
		}

		private static void Sleep()
		{
			Thread.Sleep( 3000 );
		}

		private void TypingBtn_Click( object sender, EventArgs e )
		{
			Sleep( );
			Typing.DoTypingTest( );
		}


		private void MouseMovingBtn_Click( object sender, EventArgs e )
		{
			Sleep( );
			MouseMoving.DoMouseMovingTest( );
		}

		private void ScrollingBtn_Click( object sender, EventArgs e )
		{
			Sleep( );
			Scrolling.DoScrollingTest( );
		}
	}
}
