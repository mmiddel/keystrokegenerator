﻿namespace KeyStrokeGenerator
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.TypingBtn = new System.Windows.Forms.Button();
			this.ScrollingBtn = new System.Windows.Forms.Button();
			this.MouseMovingBtn = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// TypingBtn
			// 
			this.TypingBtn.Location = new System.Drawing.Point(13, 13);
			this.TypingBtn.Name = "TypingBtn";
			this.TypingBtn.Size = new System.Drawing.Size(129, 58);
			this.TypingBtn.TabIndex = 0;
			this.TypingBtn.Text = "Typing";
			this.TypingBtn.UseVisualStyleBackColor = true;
			this.TypingBtn.Click += new System.EventHandler(this.TypingBtn_Click);
			// 
			// ScrollingBtn
			// 
			this.ScrollingBtn.Location = new System.Drawing.Point(24, 91);
			this.ScrollingBtn.Name = "ScrollingBtn";
			this.ScrollingBtn.Size = new System.Drawing.Size(157, 82);
			this.ScrollingBtn.TabIndex = 1;
			this.ScrollingBtn.Text = "Scrolling";
			this.ScrollingBtn.UseVisualStyleBackColor = true;
			this.ScrollingBtn.Click += new System.EventHandler(this.ScrollingBtn_Click);
			// 
			// MouseMovingBtn
			// 
			this.MouseMovingBtn.Location = new System.Drawing.Point(114, 192);
			this.MouseMovingBtn.Name = "MouseMovingBtn";
			this.MouseMovingBtn.Size = new System.Drawing.Size(96, 53);
			this.MouseMovingBtn.TabIndex = 2;
			this.MouseMovingBtn.Text = "Move Mouse";
			this.MouseMovingBtn.UseVisualStyleBackColor = true;
			this.MouseMovingBtn.Click += new System.EventHandler(this.MouseMovingBtn_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 262);
			this.Controls.Add(this.MouseMovingBtn);
			this.Controls.Add(this.ScrollingBtn);
			this.Controls.Add(this.TypingBtn);
			this.Name = "MainForm";
			this.Text = "MainForm";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button TypingBtn;
		private System.Windows.Forms.Button ScrollingBtn;
		private System.Windows.Forms.Button MouseMovingBtn;
	}
}