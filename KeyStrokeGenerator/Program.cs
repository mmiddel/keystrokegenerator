﻿using System;
using System.Threading;

namespace KeyStrokeGenerator
{
	internal class Program
	{
		public static void Main()
		{
			var form = new MainForm( );
			form.ShowDialog( );
		}
	}
}