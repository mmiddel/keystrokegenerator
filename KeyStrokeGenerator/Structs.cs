﻿using System;
using System.Runtime.InteropServices;

namespace KeyStrokeGenerator
{
	class Structs
	{
		[StructLayout( LayoutKind.Sequential )]
		internal struct MOUSEINPUT
		{
			internal int dx;
			internal int dy;
			internal int mouseData;
			internal Enums.MOUSEEVENTF dwFlags;
			internal uint time;
			internal UIntPtr dwExtraInfo;
		}
		

		[StructLayout( LayoutKind.Sequential )]

		public struct INPUT
		{
			internal uint type;
			internal InputUnion U;
			internal static int Size
			{
				get { return Marshal.SizeOf( typeof( INPUT ) ); }
			}
		}

	
		[StructLayout( LayoutKind.Explicit )]
		internal struct InputUnion
		{
			[FieldOffset( 0 )]
			internal MOUSEINPUT mi;
			[FieldOffset( 0 )]
			internal KEYBDINPUT ki;
			[FieldOffset( 0 )]
			internal HARDWAREINPUT hi;
		}
		[StructLayout( LayoutKind.Sequential )]
		internal struct KEYBDINPUT
		{
			internal Enums.VirtualKeyShort wVk;
			internal Enums.ScanCodeShort wScan;
			internal Enums.KEYEVENTF dwFlags;
			internal int time;
			internal UIntPtr dwExtraInfo;
		}

		[StructLayout( LayoutKind.Sequential )]
		internal struct HARDWAREINPUT
		{
			internal int uMsg;
			internal short wParamL;
			internal short wParamH;
		}
	}
}