﻿using System.Threading;

namespace KeyStrokeGenerator
{
	internal class Scrolling
	{
		public static void DoScrollingTest()
		{
			int y = 1;
			for ( var i = 0; i < 100; i++ )
			{
				if ( i % 8 == 0 )
				{
					//simulate the user moving his / her finger back to the original position
					Thread.Sleep( 1000 );
				}

				//Scroll up or down
				if ( i % 64 == 0)
				{
					y*=-1;
				}
				Scroll( 1 * y );
				Thread.Sleep( 50 );
			}
		}

		private static void Scroll(int clicks)
		{
			var inputs = new[] { new Structs.INPUT
				{
					type = (uint)Enums.INPUTTYPES.MOUSE,
					U = new Structs.InputUnion()
						{
							mi = new Structs.MOUSEINPUT(  )
								{
									mouseData = clicks * Constants.MOUSE_DELTA,
									dwFlags = Enums.MOUSEEVENTF.WHEEL,
								}
						}
				} };

			User32.SendInput( 1u, inputs, Structs.INPUT.Size );
		}
	}
}