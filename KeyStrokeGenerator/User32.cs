﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace KeyStrokeGenerator
{
	class User32
	{

		[DllImport( "user32.dll" )]
		internal static extern uint SendInput( uint nInputs, 
			[MarshalAs(UnmanagedType.LPArray), In] Structs.INPUT[] pInputs, 
			int cbSize
		);
			

	}
}
